#ifndef SISCMEMORY_HPP_INCLUDED
#define SISCMEMORY_HPP_INCLUDED

#include <stdint.h>

const int MEM_SIZE = 65536;

typedef uint16_t adr_t;
class SiscMemory {
    
    private:
        uint8_t* mem_pointer; 
    public:
        SiscMemory(bool initialize=false);
        uint8_t read_byte(adr_t adr);
        uint16_t read_word(adr_t adr);
        void write_byte(adr_t adr, uint8_t value);
        void write_byte(adr_t adr, uint16_t value);
        void write_word(adr_t adr, uint16_t value);
        
};













#endif // SISCMEMORY_HPP_INCLUDED
