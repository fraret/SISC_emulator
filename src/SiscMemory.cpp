#include "SiscMemory.hpp"

SiscMemory::SiscMemory(bool initialize) {
    /**
     * Creates and, if specified, initializes the memory.
     * 
     * @param initialize Wheter to initialize the memory with 0s or leave it as given
     */
    
    if (initialize) mem_pointer = new uint8_t[MEM_SIZE](); //initialize with zeros
    else mem_pointer = new uint8_t[MEM_SIZE]; //only set pointer
    
}

uint8_t SiscMemory::read_byte(adr_t adr) {
    /**
     * Reads a byte from memory
     * 
     * @param adr Address
     * @return Value of that position in memory
     */
    return mem_pointer[adr];
}

uint16_t SiscMemory::read_word(adr_t adr) {
    /**
     * Reads a word (16 bits) from memory
     * 
     * @param adr Address, if odd it will be decremented by 1 before accessing the memory
     * @return Value interpreted as little-endian
     */
    
    if (adr%2) --adr; //ensure adr is even and round to nearest even if not
    
    uint16_t temp = mem_pointer[adr+1];
    temp <<= 8;
    temp += mem_pointer[adr];
    return temp;
}

void SiscMemory::write_word(adr_t adr, uint16_t value) {
    /**
     * Stores a word (16 bits) into memory
     * 
     * @param adr Address, if odd it will be decremented by 1 before accessing the memory
     * @param value Value to be stored in memory
     */
    
    if (adr%2) --adr; //ensure adr is even and round to nearest even if not
    
    mem_pointer[adr] = uint8_t(value%256);
    mem_pointer[adr+1] = uint8_t(value>>8);
}

void SiscMemory::write_byte(adr_t adr, uint16_t value) {
    /**
     * Stores a byte into memory from a word
     * 
     * @param adr Address
     * @param value Value. Only the lower 8 bits will be stored.
     */
    SiscMemory::write_byte(adr,uint8_t(value%256));
}

void SiscMemory::write_byte(adr_t adr, uint8_t value) {
    /**
     * Stores a byte into memory
     * 
     * @param adr Address
     * @param value Value
     */
    mem_pointer[adr] = value;
}
