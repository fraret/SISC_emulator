#ifndef SISCCOMPUTER_HPP_INCLUDED
#define SISCCOMPUTER_HPP_INCLUDED

#include "SiscMemory.hpp"
#include <stdint.h>
#include <unordered_set>
#include <array>


class SiscComputer {
    
    private:
        SiscMemory* memory;
        std::array<uint16_t, 8> regs;
        uint16_t pc;
        
        /** 
         * Executes the instruction represented by opcode.
         * Assumes pc has already been incremented by 2.
         * 
         * @param opcode codified instruction
         * @return true if opcode is a valid instruction, false otherwise
         */
        bool execute_code(uint16_t opcode);
        bool allow_odd_pc=false;
        
    public:
        
        SiscComputer(SiscMemory* new_memory);
        SiscComputer(SiscMemory* new_memory, uint16_t start_pc);
        
        
        uint16_t execute_until_pc(std::unordered_set<uint16_t> & breakpoints);
        /**
         * Steps the execution until pc equals end_pc, excluded
         * WARNING: this function WILL loop infinitely if the program
         *   never gets to instruction PC.
         * 
         * @param end_pc Instruction that will not be executed
         * 
         */
        void execute_until_pc(uint16_t end_pc);
        
        /**
         * Steps the execution until pc equals end_pc, excluded
         * WARNING: this function WILL loop infinitely if the program
         *   never gets to a NOP instruction.
         */
        void execute_until_nop();
        bool step();
        
        void set_pc(uint16_t new_pc);
        void set_reg(int reg_num, uint16_t value);
        void set_all_reg(uint16_t value=0);
        
        void load_memory(SiscMemory* new_memory);
        SiscMemory* get_memory();
        
        
};



#endif //SISCCOMPUTER_HPP_INCLUDED
