#include "tools.hpp"
#include "SiscMemory.hpp"

void memdump(SiscMemory& memory, std::string filename) {
    std::ofstream outstream;
    outstream.open(filename, std::ios_base::binary);
    for (int i = 0; i < MEM_SIZE; ++i){
        outstream.put(memory.read_byte(i));
    }
    outstream.close();
}

void dump_n_bytes(SiscMemory& memory, std::string filename, uint16_t st_adr, int len) {
    if (len > MEM_SIZE)  throw("len > MEM_SIZE");
    std::ofstream outstream;
    outstream.open(filename, std::ios_base::binary);
    for (int i = 0; i < len; ++i){
        outstream.put(memory.read_byte(st_adr));
        ++st_adr;
    }
    outstream.close();
}

void memread(std::string filename, SiscMemory& memory, uint16_t st_adr) {
    std::ifstream infile;
    infile.open(filename, std::ios_base::binary);
    while(infile.peek() != EOF) {
            memory.write_byte(st_adr,uint8_t(infile.get()));
            ++st_adr;
    }
}

