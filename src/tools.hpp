#ifndef TOOLS_HPP_INCLUDED
#define TOOLS_HPP_INCLUDED

#include<fstream>
#include "SiscMemory.hpp"

/**
 * Dumps the whole SiscMemory to a file.
 * 
 * @param memory The SiscMemory to Dump
 * @param filename String with the filename to which the memory will be dumped
 */
void memdump(SiscMemory& memory, std::string filename);

/**
 * Dumps len bytes starting from std_adr included from a SiscMemory to a file.
 * 
 * @param memory The SiscMemory to Dump
 * @param filename String with the filename to which the memory will be dumped
 * @param st_adr Starting address, included
 * @param len Number of bytes to dump
 * 
 * @throws Exception If len > MEM_SIZE
 */
void dump_n_bytes(SiscMemory& memory, std::string filename, uint16_t st_adr, int len);

/**
 * Reads file filename and loads its contents to memory, starting at st_addr 
 * and oveflowing to 0x0000 
 * 
 * @param filename Name of the file to read
 * @param memory Pointer to the memory which will be written
 * @param st_addr Adress where the first byte will be loaded
 */
void memread(std::string filename, SiscMemory& memory, uint16_t st_adr=0x0000);




#endif // TOOLS_HPP_INCLUDED
