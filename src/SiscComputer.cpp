#include <iostream>
#include "SiscComputer.hpp"


SiscComputer::SiscComputer(SiscMemory* new_memory, uint16_t start_pc) {
    /**
     * Initializes a new computer with the RAM memory passed and initializes
     * pc to the value given
     * 
     * @param new_memory SiscMemory object to serve as RAM
     * @param start_pc Starting PC value
     */
    load_memory(new_memory);;
    set_pc(start_pc);
}

SiscComputer::SiscComputer(SiscMemory* new_memory) {
    /**
     * Initializes a new computer with the RAM memory passed
     * 
     * @param new_memory SiscMemory object to serve as RAM
     */
    load_memory(new_memory);
}

void SiscComputer::set_reg(int reg_num, uint16_t value) {
    /**
     * Sets register #reg_num to value
     * 
     * @param reg_num register number. 0 <= reg_num <= 7
     * @param value Value to set register
     * @throw Exception String if reg_num invalid
     */
    
    if(reg_num < 0 or reg_num > 7) throw ("Reg_num out of bounds");
    regs[reg_num] = value;
}

void SiscComputer::set_all_reg(uint16_t value) {
    /**
     * Sets all registers to value
     * 
     * @param value Value to set register
     */
    for(int i = 0; i < 8; ++i) regs[i]=value;
}

void SiscComputer::set_pc(uint16_t new_pc) {
    /**
     * Sets PC to the value passed.
     * 
     * @param new_pc Value to which pc will be set. 
     *   If allow_odd_pc is false, new_pc must be even or an exception will be thrown
     * @throw Exception String if pc odd and allow_odd_pc is false
     */
    if (allow_odd_pc) pc = new_pc;
    else {
        if (pc%2) throw ("Uneven pc");
        else pc = new_pc;
    }
}

void SiscComputer::load_memory(SiscMemory* new_memory) {
    /**
     * Changes the RAM memory to the one pointed by new_memory
     * 
     * 
     * @param new_memory Pointer to memory to use
     */
    memory = new_memory;
}

bool SiscComputer::step() {
    /**
     * Steps one cycle in executing the program, incrementing pc
     * 
     */
    uint16_t temp_pc = pc;
    pc += 2;
    return execute_code(memory->read_word(temp_pc));
    
}


void SiscComputer::execute_until_pc(uint16_t end_pc) {
    while (pc != end_pc) {
        step();
    }
}

void SiscComputer::execute_until_nop() {
    do {
        //std::cerr << std::hex << pc << std::endl;
    } while (step()) ;
}



inline uint8_t get_instr_type(uint16_t opcode) {
    return opcode>>12;
}

uint16_t sgn6(uint16_t x) {
    if (x & 0x0020) return x | 0xFFC0;
    return x;
}

void get_abdf(uint16_t opcode, uint8_t& a, uint8_t& b, uint8_t& d, uint8_t& f) {
    /* Helper function for decoding 4-whatever instruction format
     * 
     */
    f = opcode & 0x07;
    d = (opcode>>3) & 0x07;
    b = (opcode>>6) & 0x07;
    a = (opcode>>9) & 0x07;
}

void get_abn(uint16_t opcode, uint8_t& a, uint8_t& b, uint16_t& n) {
    /* Helper function for decoding 4-whatever instruction format
     * 
     */
    n = sgn6(opcode & 0x003F);
    
    b = (opcode>>6) & 0x07;
    a = (opcode>>9) & 0x07;
}

void get_a(uint16_t opcode, uint8_t& a, bool& bit){
    a = (opcode>>9) & 0x07;
    bit = bool(opcode | 0x0100);
}

inline uint16_t inv_sgn(uint16_t x) {
    return (~x)+1;
}

inline bool is_negative(uint16_t x) {
    return x & 0x8000;
}

uint16_t shl(uint16_t x, uint16_t y) {
    if (is_negative(y)) {
        y = inv_sgn(y);
        return x>>y;
    } else return x<<y;
}

uint16_t sha(uint16_t x, uint16_t y) {
    if (is_negative(y)) {
        y = inv_sgn(y);
        if (is_negative(x)){
            x = inv_sgn(x);
            x = x>>y;
            x = inv_sgn(x);
            return x;
        } else return x>>y; 
    } else return x<<y;
}

uint16_t al_op(uint8_t f, uint16_t x, uint16_t y) {
    switch (f) {
        case 0:
            return x & y; //AND
        case 1:
            return x | y; //OR
        case 2:
            return x ^ y; //XOR
        case 3:
            return ~x; //NOT
        case 4:
            return x + y; //ADD
        case 5:
            return x - y; //SUB
        case 6:
            return sha(x,y);
        case 7:
            return shl(x,y);
        default:
            throw("f out of bounds in al_op");
    }
}

bool cmplt(uint16_t x, uint16_t y){
    if(is_negative(x) and is_negative(y)) return x < y;
    else if (is_negative(x) and (not is_negative(y))) return true;
    else if ((not is_negative(x)) and is_negative(y)) return false;
    else return x < y;
}

uint16_t cmp_op(uint8_t f, uint16_t x, uint16_t y) {
    /**
     * Gives the result of performing the comparation function f on x, y
     * 
     * @param f The function to 
     */
    bool res = false;
    switch (f) {
        case 0:
            res = cmplt(x,y);
            break;
        case 1:
            res = cmplt(x,y) or x == y;
            break;
        case 2:
            break;
        case 3:
            res = x == y;
            break;
        case 4:
            res = x < y;
            break;
        case 5:
            res=  x <= y; //SUB
        case 6:
        case 7:
            break;
        default:
            throw("f out of bounds in cmp_op");
    }
    if (res) return 0x0001;
    return 0x0000;
}

uint16_t sgn8(uint16_t opcode) {
    /**
     * Extends the sign of the last 8 bits of opcode
     * 
     * @param opcode Instruction
     * @return last 8 bits of opcode sign-extended to 16 bits
     */
    uint16_t offset = opcode & 0x00FF;
    if (opcode & 0x0080) offset = offset | 0xFF00;
    return offset;
    
}

/**
 * Extends the sign of the last 8 bits of an instruction and multiplies the 
 * result by 2
 * 
 * @param opcode Instruction
 * @return last 8 bits of opcode sign-extended and multiplied by 2
 */
inline uint16_t sgn8x2(uint16_t opcode) {

    return sgn8(opcode)<<1;
}


bool SiscComputer::execute_code(uint16_t opcode) {
    //std::cerr << std::hex << opcode << std::endl;
    
    uint8_t instruction_type = get_instr_type(opcode);
    
    //std::cerr << std::dec << int(instruction_type) << std::endl;
    
    if (instruction_type == 0 or instruction_type == 1) { //AL or CMP
        uint8_t a,b,d,f;
        get_abdf(opcode,a,b,d,f);
        if (instruction_type == 0) regs[d] = al_op(f,regs[a],regs[b]);
        else regs[d] = cmp_op(f,regs[a],regs[b]);
    } else if (instruction_type == 2) { //ADDI
        uint8_t a,d;
        uint16_t n;
        get_abn(opcode,a,d,n);
        regs[d] = regs[a] + n;
    } else if ( 3 <= instruction_type and instruction_type <= 6) {
        //MEMORY, TODO
        
    } else if (instruction_type == 7){
        //JALR, TODO
        
    } else if (instruction_type == 8) {
        bool bnz;
        uint8_t a;
        get_a(opcode,a,bnz);
        bool branch = false; 
        if (bnz) { 
            branch = regs[a] != 0;
        } else {
            branch = regs[a] == 0;
        }
        if (branch) pc += sgn8x2(opcode);
    } else if (instruction_type == 9) {
        bool hi;
        uint8_t d;
        uint16_t n = sgn8(opcode);
        get_a(opcode,d,hi);
        if (hi) {
            regs[d] = (regs[d] & 0x00FF) | (n<<8);
        } else {
            regs[d] = n;
        }
    } else if (instruction_type == 10) {
        bool out;
        uint8_t a;
        get_a(opcode,a,out);
        if (out) {
            std::cout << std::hex << "[0x" << (opcode & 0x00FF) << "]: " << "0x" 
            << regs[a] << std::endl;
        } else {
            //TODO implement input, non-trivial
            throw("Input not yet implemented");
        }
    } else return false;
    return true;
}


