#include <iostream>
#include "SiscComputer.hpp"
#include "strings.hpp"
#include "SiscMemory.hpp"
#include "tools.hpp"

int main(int argc, char **argv) {
    std::cout << ABOUT_LINES << std::endl;
    SiscMemory memory(true);
    memread("mem.hex", memory);
    SiscComputer computer(&memory);
    computer.execute_until_nop();
    return 0;
}
